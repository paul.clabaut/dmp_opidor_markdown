### Licence : / *Licence:*

Ce document est mis à disposition selon les termes de la licence Creative Commons Atttribution – Partage à l'identique 4.0 International http://creativecommons.org/licenses/by-sa/4.0 si vous souhaitez faire évoluer son contenu.
Dans le cas où vous souhaitez utiliser ce document comme trame pour réaliser votre propre Plan de Gestion de logiciel, ce document est mis à disposition selon les termes de la licence Creative Commons Attribution 4.0 International http://creativecommons.org/licenses/by/4.0.
Merci de citer ce travail comme suit : T. Gomez-Diaz, G. Romier, Modèle de Plan de Gestion de Logiciel de la Recherche V3.2, Projet PRESOFT, avril 2018.


*This document is published under the Creative Commons Attribution-ShareAlike 4.0 International licence http://creativecommons.org/licenses/by-sa/4.0 in the case you wish to contribute to new template versions.
The licence Creative Commons Attribution 4.0 International (CC BY 4.0) http://creativecommons.org/licenses/by/4.0 applies if you use this document as a template to realise your own SMP.
How to cite this work: T. Gomez-Diaz, G. Romier, Research Software Management Plan template V3.2,  PRESOFT project, April 2018.*

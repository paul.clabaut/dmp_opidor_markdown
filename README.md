# Plan de Gestion du Logiciel [nom de votre logiciel]
    V1.0, [date de création de ce document, date de dernière mise à jour]
    [Auteur(s)]

# *Research Software Management Plan of [your software name]*
    *V1.0, [date of creation, last revision date]*
    *[Author(s)]*

[[_TOC_]]

### 1. Métadonnées / *Metadata*

Ce jeu de métadonnées est donné à titre d'exemple. L’idée est d’indiquer seulement ce qui décrit le logiciel et permet de le retrouver et caractériser, par exemple : nom, site web, lien code, contact, version et date, licence, auteurs et affiliations, mots-clés scientifiques et techniques.

*This metadata set is given as an example. The idea is to give the minimal set of informations to describe the software, as for example: name, web site, link to the code, contact, version, date, licence, developers and their affiliations, scientific and technical keywords.*

|1.1 Métadonnées / *Metadata* | |
|:------------------------------------|:-------------------------------------------------------|
|  Nom du logiciel  <br/> *Software Name* | S’il faut choisir un nom, éviter les noms de marques déposées ou les noms d’autres logiciels. <br/> *If you need to choose a name, avoid the name of a brand and other software names.* |
|  Description courte du logiciel <br/> *Short software description* | Une courte phrase qui décrit votre logiciel <br/> *A short sentence describing your software* |
|  Site ou page web du logiciel <br/> *Software web page or website* | Lien vers le code source ou le package <br/> *Link to source code or package* |
|  Contact <br/> *Contact* | Adresse mail <br/> *Email address* |
|  Laboratoire en charge du logiciel <br/> *Research unit in charge of the software* | <br/> |
|  Développeurs principaux du logiciel et leurs affiliationsi <br/> *Main developers and their affiliations* | <br/> |
|  Version du logiciel <br/> *Software version* | Exemple :  V1 <br/> *Example: V1* |
|  Date de la version du logiciel <br/> *Date of the software version* | (AAAA-MM-JJ) <br/> *(YYYY-MM-DD)* |
|  Licence <br/> *Licence*  | <br/> | 
|  Domaine scientifique <br/> *Scientific discipline* | Suivant par exemple la classification des disciplines utilisées par l’ERC ou celle utilisée par EGI  <br/> *For example according to the ERC or to the EGI scientific classification.* |
|  Fonctionnalités importantes du logiciel <br/> *Main functionalities* | Sous forme de mots-clés <br/> *In the form of keywords* |
|  Caractéristiques techniques importantes <br/> *Main technical characteristics* | Sous forme de mots-clés <br/> *In the form of keywords* | 
|  Autres mots-clés <br/> *Other keywords* | <br/> |

### 2. Contexte du logiciel / *Software context*

Éléments de contexte du logiciel, à compléter au fur et à mesure des nouvelles informations.

*Here are some items to be filled following the software evolutions.*

|  2.1 Historique / History  | |
|:------------------------------------|:-------------------------------------------------------|
|  Matériel préparatoire <br/> *Preparatory material* | Identifier et dater le matériel préparatoire. <br/> *Identify and date the preparatory material.*  |
|  Cahier de charges (s’il y en a un), modèle de conception (UML ou autre), cas d'utilisation… <br/> *Specifications (if any), conception model (UML or other), use cases...* | Références et dates du cahier des charges, du modèle de conception... <br/> *References and dates for the specifications, conception model...*  |
|  Versions précédentes du logiciel <br/> *Previous software versions*  | Identifier et dater les différentes versions. <br/> *Identify and date the previous versions.*  |
|  Composants intégrés dans le logiciel et dépendances externes <br/> *Components included in the software and external dependencies*  | Identifier et décrire les différents composants dont le logiciel est successeur direct ou indirect : nom, version, date, auteurs, site web, licence… <br/> *Identify and describe the different components that are part of the software: name, version, date, authors, website, licence...*  |
|  Nouveaux composants à intégrer dans la nouvelle version du logiciel <br/> *New components to be included in the new version of the software*  | Identifier et décrire ces nouveaux composants <br/> *Identify and describe the different components*  |
|  Feuille de route du logiciel <br/> *Roadmap*  |  Lien <br/> *Link*  |
|  Existe  t-il des logiciels aux fonctionnalités équivalentes, quelles sont les différences ? <br/> *Are there other software developments with similar functionalities? Which are the differences?*  |  <br/> |
|  Publications, données et autres productions associées. <br/>  *Publications, data and other associated productions.*  |  Par exemple : publications de l'équipe pour présenter le logiciel ou des résultats scientifiques obtenus en utilisant le logiciel. <br/>  *For example: the team publications to explain the software design or to show the obtained scientific results by using the software.*  |
|  A cette date (à préciser), quel est le coût estimé du développement du logiciel ? <br/> *Up to this date (to be given), estimation of the software’s cost.*  |  En nombre de personnes/mois par exemple. <br/> *Number of person/months for example.*  |


|  2.2 Projet(s) lié(s) au logiciel <br/> *Project(s) related to the software*  |  Note : il peut y avoir plusieurs projets successifs pour un même logiciel. Dans ce cas, il faudrait dupliquer le tableau pour chacun. <br/> *Note: there can be several successive projects for a same software. In this case you can duplicate this table for each project.*  |
|:------------------------------------|:-------------------------------------------------------|
|  Nom du projet <br/> *Project’s name*  |  Peut être différent du nom du logiciel. <br/> *The project’s name may be different from the software’s name.*  |
|  Type du projet (ANR, H2020…). <br/> *Type of the project (ANR, H2020…).*  | <br/>  |
|  Identifiant  <br/> *Identifier* | <br/>  |
|  Date de début du projet <br/> *Start date of the project*  | (AAAA-MM-JJ) <br/> *(YYYY-MM-DD)*  |
|  Date de fin du projet <br/> *End date of the project*  | (AAAA-MM-JJ) <br/> *(YYYY-MM-DD)*  |
|  Site web du projet <br/> *Project website*  | <br/> |
|  Cadre dans lequel est développé ce logiciel <br/> *Framework in which the software is developed*  |  Par exemple le logiciel peut être l’objectif du projet ou bien il peut être développé dans un ou plusieurs work packages. Les développements antérieurs doivent être signalés lors de la rédaction de l’accord de consortium. Définir les nouveaux développements dans le cadre de ce projet. <br/> *For example, the software may be the objective of the project or may be developed in a work package. Previous developments must be declared in the grant agreement or consortium agreement. Define the new developments in this project.*  |
|  Ressources allouées (humaines, financières et matérielles) <br/> *Allocated resources and funding (human, financial, hardware)*  | <br/> |
|  Éventuelles contraintes liées au projet <br/> *Possible constraints linked to the project*  |  Par exemple indications dans le « Grant agreement » ou l’accord de Consortium sur la licence des développements du projet. <br/> *For example mandatory licence for the projects developments in the Grant agreement or in the consortium agreement.*  |
|  Partenaires et rôle par rapport au logiciel <br/> *Partners and role in relation to the software*  | <br/>  |
|  Le logiciel est-il un livrable du projet ? <br/> *Is the software a deliverable of the project?*  | <br/> |

Note : un stage peut être vu comme un projet qui apporte des ressources humaines, dans ce cas dupliquer le tableau précédent pour y indiquer les dates et les ressources associées. 

*Note: a traineeship can be considered as a project to bring human resources. In this case duplicate the previous table to indicate the associated dates and resources.*


|  2.3 Questions légales et politique de diffusion <br/> *Legal issues and distribution policy* | <br/>  |
|:------------------------------------|:-------------------------------------------------------|
|  Propriété intellectuelle <br/> *Intellectual property*  |  Identifier les auteurs et les détenteurs des droits patrimoniaux.<sup>1</sup> <br/> *Identify authors, rightholders.*<sup>1</sup>  |
|  Formule de droits d’auteurs ou copyright <br/> *Rightholders or copyright statement*  | <br/>  |
|  Politique de diffusion <br/> *Distribution policy*  |  Contraintes du ou des projets, des partenaires et de leurs organismes. <br/> *Constraints linked to the project(s), the partners and their organisms.*  |
|  Licence(s) <br/> *Licence(s)* |  Attention à l’héritage et la compatibilité des licences si nécessaire. Indiquer aussi la licence de la documentation, du site web... <br/> *Beware of possible heritage and licence compatibility issues. Mention the licences of the documentations, of the website...*  |
|  Si ouverture du code, quand est-elle prévue ? <br/> *If the code is open, when will it be open?*  |  À valider avec les éventuels partenaires et les contraintes de l’éventuel financement. <br/> *To be validated with the possible partners and according to the constraints linked to the funding.*  |
|  Gestion de la propriété intellectuelle des possibles collaborations externes <br/> *Management of the intellectual property of external contributions*  |  Contrat de cession de droits à prévoir. <br/> *Rights' transfer agreement to be planned.*  |
|  Clauses ou mesures de confidentialité et traitement de données sensibles <br/> *Non disclosure or privacy clauses and sensitive data processing*  |  Si besoin. <br/> *If needed.*  |

<sup>1</sup> Dans le cadre d’un projet la propriété intellectuelle est décrite dans l’accord de consortium.
<sup>1</sup> *In the framework of a project the intellectual property is part of the consortium agreement.*

### 3. Caractéristiques du logiciel / *Software features*


|  3.1 Objectifs scientifiques <br/> *Scientific goals*  |  <br/> |
|:------------------------------------|:-------------------------------------------------------|
|  Objectifs, résultats attendus <br/> *Objectives, expected results*  | Décrire de façon synthétique les objectifs scientifiques liés au logiciel ainsi que les résultats attendus. <br/> *Describe in a synthetic way the scientific goals and the expected results linked to the software.*  |


|  3.2 Objectifs d'utilisation et de diffusion <br/> *Usage and distribution objectives*  | <br/> |
|:------------------------------------|:-------------------------------------------------------|
|  Durée de vie prévue ou envisagée <br/> *Planned or considered lifespan*  | <br/>  |
|  Utilisation prévue <br/> *Planned usage*  | Pour quoi faire (publications, formation, utilisation en production, industrialisation) ? <br/> *What for (publications, teaching, production level usage, industry level usage)?*  |
|  Public prévu ou ciblé <br/> *Target public*  |  Par exemple : chercheur, équipe, diffusion restreinte, collaboration, diffusion large… <br/> *For example: researchers, team, restricted distribution, collaboration, wide distribution...*  |
|  Support prévu pour les utilisateurs  <br/>  *Planned user support*  |  Type de support, moyen utilisé, ressources et qualité de service annoncée aux utilisateurs. Par exemple, support à l’utilisation, système de tickets, une personne en “best effort”… <br/> *Type of support, tools, resources, quality of service for the users. For example, user support, ticket system, a person in “best effort”...*  |
|  Objectifs de diffusion <br/> *Distribution goals*  | Le logiciel est “seulement” un outil interne, le logiciel fera l'objet d'une publication, le logiciel sera diffusé largement… <br/> *The software is “for internal use only”, the software will be published via an article, the software will be distributed widely...*  |
|  Communauté de collaboration souhaitée <br/> Collaboration community wished  | Oui / Non / Ne sait pas encore. <br/> Si oui, laquelle? Par exemple dans la communauté scientifique ciblée. <br/> *Yes / No / Don’t know.* <br/> *If yes, which one? For example in the target scientific community.*  |
|  Adéquation des ressources (développement, maintenance...) aux objectifs de diffusion <br/> *Adequacy of the resources  (development, maintenance...) to the distribution goals*  | Les ressources disponibles sont-elles suffisantes (ressources humaines, financières et matérielles) ? <br/> *Are the available resources suitable? (human, financial and material resources) ?*  |
|  Étude de risques <br/> *Risk analysis*  | Une étude de risques peut être utile avant de se lancer dans un développement coûteux ou dans une diffusion pas très réfléchie. <br/> *A risk analysis may be useful before launching an expensive development or an unwise distribution.*  |
|  Préservation du logiciel <br/> *Software preservation*  | Quel est l’objectif de préservation et la solution utilisée ? Note : on parle de sauvegarde à court terme, et d'archivage à long terme. <br/> *What is the objective for the preservation and what is the solution used? Please distinguish short term backup and long term archiving.*  |


|  3.3 Caractéristiques techniques <br/> *Technical features*  | <br/> |
|:------------------------------------|:-------------------------------------------------------|
|  Technologies utilisées <br/> *Used technologies*  | <br/> |
|  Dépendances <br/> *Dependencies*  | Système d'exploitation, SDK, bibliothèques, navigateur, API externes… <br/> *OS, SDK, libraries, browser, external APIs...*  |
|  Réutilisation de briques existantes <br/> *Already existing components reuse*  | Contraintes techniques <br/> *Technical constraints*  |
|  Documentation <br/> *Documentation*  | Indiquer l’url de la documentation. <br/> *Give the documentation's url.*  |
|  Normes et standards utilisés <br/> *Used norms and standards*  |  Exemple : norme ISO du language de développement. <br/> *Example: ISO norm of the development language.*  |


### 4. Organisation de l’équipe / *Team organisation*


|  4.1 Organisation de l’équipe <br/> *Team organisation*  | <br/> |
|:------------------------------------|:-------------------------------------------------------|
|  Gouvernance <br/> *Governance*  | <br/> |
|  Accord de consortium incluant la gouvernance, le développement mais aussi le futur du logiciel <br/> *Consortium agreement including governance, development and future of the software*  | <br/> |
|  Constitution de l'équipe <br/> *Team*  | <br/> |
|  Organisation autour du logiciel <br/>  *Organisation around the software*  | <br/> |
|  Répartition des coûts et financements <br/> *Costs and funding distribution*  | <br/> |
|  Type de développement <br/>  *Type of development*  | <br/> |
|  Actions à prévoir en cas de départ d’une personne <br/> *Actions to be planned in case of a person's leave*  | <br/> |


### 5. Organisation du développement / *Development organisation*


|  5.1 Organisation du développement <br/> *Development organisation*  | <br/> |
|:------------------------------------|:-------------------------------------------------------|
|  Équipe de développement <br/> *Development team*  | Sur un site, éclatée sur plusieurs sites, avec une ou plusieurs tutelles. <br/> *On one or several sites, depending on one or several institutions.*  |
|  Plan de développement <br/> *Development plan*  | Planification des versions futures en indiquant les nouvelles fonctionnalités prévues, les dates. <br/> *Roadmap including the new versions, functionalities planned and dates.*  |
|  Méthodes de développement, standards utilisés, outils liés et infrastructure (dépôt de code) <br/> *Development methods, used standards, tools and infrastructures (code repository)*  | Exemple : outils de gestion de version et de développement collaboratif. <br/> *Example: tools for version management and collaborative development.*  |
|  Responsabilités des acteurs dans le développement <br/> *Actors’ responsibilities in the development*  | <br/> |
|  Procédures qualité <br/> *Quality procedures*  | Par exemple les mesures prises pour faciliter la maintenabilité du logiciel, les bonnes pratiques appliquées, les tests de vérification … <br/> *For example, actions taken to foster the software maintainability, best practices applied, verification tests...*  |
|  Sécurité (prise en compte dans le développement) <br/> *Security (taken into account in the development)*  | <br/> |
|  Organisation de la production des versions, de la gestion des bugs, des tests et de la validation <br/> *Version delivery, bugs, tests and validation management*  | Y a-t-il des tests ou autres procédures de validation ? Quel en est le suivi ? Sont-ils fournis aux utilisateurs finaux? Comment sont traités les bugs ? <br/>  *Are there testing or other validation procedures? With which follow-up? Are they to be given to final users? How do you manage bugs?*  |
|  Organisation de la production de la documentation (interne et utilisateurs, installation et prérequis, exemples d’utilisation) <br/> *Documentation production management  (internal and for users, installation and requirements, use examples)*  |  Expliquer la façon dont la documentation est produite et mise à jour à chaque version (responsabilité, organisation…) <br/> *Explain how the documentation is produced and updated for each version (responsibilities, organisation…)*  |
|  Décrire les principales évolutions prévues <br/> *Describe main planned evolutions*  | Par exemple : intégration dans d'autres projets, traductions… <br/> *For example: integration in other projects, software translations...*  |
|  Si une participation extérieure est souhaitée et possible, quelles en sont les règles (validation des apports, intégration des apports dans les versions majeures, intégration de la participation) ? <br/> *If external participations are expected and possible, which are the rules (validation of the contributions, contribution integration in the major versions, participation integration)?*  |  Il est souhaitable de définir précisément les règles avant toute participation extérieure. <br/>  *It is advisable to define accurately the rules before any external participation.*  |


### 6. Organisation de la diffusion / *Distribution organisation*


|  6.1 Organisation de la diffusion <br/> *Distribution organisation*  | <br/> |
|:------------------------------------|:-------------------------------------------------------|
|  Entrepôt de référence <br/> *Reference repository*  | Par exemple le lien vers la version de votre logiciel sur SourceSup, Zenodo ou Gitlab IN2P3. <br/> *For example the link to the software version on SourceSup, Zenodo or Gitlab IN2P3.*  |
|  Identifiant pérenne <br/> *Persistent identifier*  | Indiquer par exemple le DOI de votre logiciel. <br/> *Indicate for example the DOI of your software.*  |
|  Formule de citation <br/> *Citation form*  | Vous pouvez proposer de citer la publication qui décrit votre logiciel ou celle qui vous semble la plus importante. Vous pouvez par exemple proposer : "auteurs(s), nom du logiciel, description courte, version, date, url". <br/> *You can suggest to cite the publication that describes your software or the one that seems to be the most important. Otherwise you may propose for example: “author(s), software name, short description, version, date, url”*  |
|  Liens vers des publications ou autres productions externes à l’équipe et qui utilisent le logiciel. <br/> *Links to articles or other research outputs external to the team and that use the software*  |  Important : montrer si le logiciel est utilisé en dehors de l’équipe de développement ou des laboratoires d'origine. <br/> *Important: to show that the software is used outside the development team or the original laboratories.*  |
|  Référencement (annonces, sites de la communauté…) <br/> *Referencing (announces, websites of the scientific community...)*  | <br/> |
|  Communications <br/> *Communications*  | Conférences, posters, flyers… <br/> *Conferences, posters, flyers...*  |
|  Publications dans un journal spécialisé <br/> *Publications in a software journal*  | <br/>  |
|  Support aux utilisateurs <br/> *User support*  | Tel que présenté aux utilisateurs. <br/> *Such as offered to the users.*  |
|  Indicateurs d’utilisation <br/> *Usage indicators*  |  Nombre de téléchargements, nombre d’échanges avec utilisateurs… <br/> *Number of downloads, number of exchanges with users...*  |


### 7. Gestion du plan de gestion / *SMP management*


|  7.1 Organisation, contraintes et suivi du plan de gestion du logiciel <br/> *SMP management, constraints and monitoring*  | <br/> |
|:------------------------------------|:-------------------------------------------------------|
|  Personne responsable de ce plan <br/> *Person in charge of this SMP*  | <br/> |
|  Le plan de gestion du logiciel est-il requis par le financement des projets, contrats ou autres ? <br/> *Is the SMP required by a project funding, an agreement, contract or other?*  | <br/> |
|  Organisation pour la rédaction et le suivi du plan de gestion du logiciel et de la réalisation des actions et objectifs <br/> *Organisation to write and update the SMP and monitor actions and goals*  | Y a-t-il un espace partagé collaboratif ? Est-ce un document texte ? Quelle est la périodicité de la mise à jour prévue ou est-ce une mise à jour continue, quels événements peuvent donner lieu à une mise à jour, les intervenants…? <br/> *Is there a collaborative place for this SMP? Is it a text document?  What is the update frequency or it is updated continuously? What type of events triggers an update? Who are the actors?*  |
|  Diffusion de ce SMP <br/> *This SMP distribution*  | Public, restreint, confidentiel, ne sait pas encore <br/> *Public, restricted, confidential, don’t know*  |
|  Liens avec le plan de gestion de données (Data Management Plan) du projet s’il y a lieu<sup>2</sup> <br/> *Links with the project’s Data Management Plan (if any)*<sup>2</sup>  | Si oui, y a-t-il un modèle de référence ou des points importants à développer particulièrement ? Ce plan de gestion du logiciel est-il une partie du plan de gestion des données du projet ? <br/> *If yes, is there a reference model or important points to develop? Is this SMP a part of the project’s DMP?*  |


<sup>2</sup> Dans le cadre de certains financements, le modèle de plan de gestion de données inclut une partie pour le logiciel mais un plan de gestion de données est focalisé sur les données, pas sur le logiciel. Il ne fournira donc pas un support adapté pour la gestion du logiciel.
<sup>2</sup> *In certain calls, the DMP template includes a section for software, but a DMP is focused on data, not on software and it is not designed for software management.*

### Remerciements / *Acknowledgements*


PRESOFT est un projet financé par le CNRS – IN2P3 avec la participation du CC-IN2P3, de l’IdGC et du LIGM (2017-2018)

*PRESOFT is a CNRS IN2P3 funded project with the participation of CC-IN2P3, IdGC, LIGM (2017-2018)*

### Références / *References*


T. Gomez-Diaz, G. Romier, Modèle de Plan de Gestion de Logiciel de la Recherche V3.2, Projet PRESOFT, janvier 2018. / *T. Gomez-Diaz, G. Romier, Research Software Management Plan template V3.2,  PRESOFT project, April 2018.*

Ajouter vos références / *Add your references here.*



